//
//  ViewController.swift
//  Xylophone
//
//  Created by Angela Yu on 27/01/2016.
//  Copyright © 2016 London App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController{
    
    // Grab the path, make sure to add it to your project!
    
    var audioPlayer: AVAudioPlayer!
     var selectedSoundFileName :String = ""
    let soundArray = ["note1","note2","note3","note4","note5","note6","note7"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }



    @IBAction func notePressed(_ sender: UIButton) {
      // print(sender.tag)
       
//        selectedSoundFileName = soundArray[sender.tag - 1]
//        print(selectedSoundFileName)
        playSound(soundFileName: soundArray[sender.tag - 1])
    
    }
    
    func playSound(soundFileName : String){
        let soundUrl = Bundle.main.url(forResource: selectedSoundFileName,withExtension: "wav")
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
            
        }
        catch{
            print(error)
        }
        audioPlayer.play()
    }

}

